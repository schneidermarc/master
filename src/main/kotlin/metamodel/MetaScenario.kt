package metamodel

class MetaScenario(
    val name: String,
    val amount: Int,
    val recording: Boolean,
    val actors: List<MetaActor>,
    val behaviors: List<MetaBehavior>,
    val environment : MetaEnvironment
) {
    override fun toString(): String {
        val builder = StringBuilder()
        builder.appendLine("Scenario: $name")
        builder.appendLine("Amount: $amount")
        builder.appendLine("Recording: $recording")
        builder.appendLine("Scenario Type: ${environment.scenarioType}")
        builder.appendLine("Weather: ${environment.weather}")
        builder.appendLine("Actors:")
        actors.forEachIndexed { index, actor ->
            when (actor) {
                is MetaEgoVehicle -> {
                    builder.appendLine("  ID: ${actor.id}")
                    builder.appendLine("  ${index + 1}. $actor")
                    builder.appendLine("     Model: ${actor.model}")
                    builder.appendLine("     Position: ${actor.position}")
                }

                is MetaOtherVehicle -> {
                    builder.appendLine("  ID: ${actor.id}")
                    builder.appendLine("  ${index + 1}. $actor")
                    builder.appendLine("     Model: ${actor.model}")
                    builder.appendLine("     Position: ${actor.position}")
                }

                is MetaMotorcade -> {
                    builder.appendLine("  ID: ${actor.id}")
                    builder.appendLine("  ${index + 1}. $actor")
                    builder.appendLine("     Amount: ${actor.amount}")
                    builder.appendLine("     Position: ${actor.position}")
                }

                is MetaCyclist -> {
                    builder.appendLine("  ID: ${actor.id}")
                    builder.appendLine("  ${index + 1}. $actor")
                    builder.appendLine("     Model: ${actor.model}")
                    builder.appendLine("     Position: ${actor.position}")
                }
            }
        }
        builder.appendLine("Behaviors:")
        behaviors.forEachIndexed { index, behavior ->
            builder.appendLine("  ${index + 1}. ${behavior.actors} $behavior ")
        }
        return builder.toString()
    }
}