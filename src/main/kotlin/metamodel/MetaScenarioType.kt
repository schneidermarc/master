package metamodel

enum class MetaScenarioType {
    JUNCTION,
    HIGHWAY,
    ROAD,
    LANE,
    DEFAULT
}