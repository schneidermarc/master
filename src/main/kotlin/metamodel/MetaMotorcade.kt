package metamodel

import model.Coordinate

class MetaMotorcade : MetaActor() {
    var amount: Int = 1
    var model: String = ""
    override var position: Coordinate = Coordinate(0.0, 0.0, 0.0, 0.0)
    override var id: Int = generateUniqueID()

    companion object {
        private var counter = 0
        private fun generateUniqueID(): Int = counter++
    }
}