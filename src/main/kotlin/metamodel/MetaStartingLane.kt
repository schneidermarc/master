package metamodel

enum class MetaStartingLane {
    FIRSTLANE,
    SECONDLANE,
    THIRDLANE,
    FOURTHLANE
}