package metamodel

sealed class MetaBehavior {

    lateinit var actors: List<MetaActor>
    var id : Int = 0

    data class MetaDrive(val speedRange: IntRange) : MetaBehavior()
    data class MetaStop(val brakeValue: Double = 1.0) : MetaBehavior()
    data class MetaWait(val timeRange: IntRange) : MetaBehavior()
    data class MetaTurn(val direction: MetaDirection) : MetaBehavior()
    data class MetaAccelerate(val accelerationRange: IntRange) : MetaBehavior()
    data class MetaChangeLane(val direction: MetaDirection) : MetaBehavior()
    data class MetaCrossingStreet(val actor: MetaActor) : MetaBehavior()
    data class MetaComingFrom(val direction: MetaDirection) : MetaBehavior()
    data class MetaGoingTo(val direction: MetaDirection) : MetaBehavior()
    data class MetaStarting(val startingLane: MetaStartingLane) : MetaBehavior()
    data class MetaOvertake(val actor: MetaActor) : MetaBehavior()
    data class MetaCatchingUp(val actor: MetaActor) : MetaBehavior()
    data class MetaInFrontOf(val actor: MetaActor) : MetaBehavior()
    data class MetaBehind(val actor: MetaActor) : MetaBehavior()
    data class MetaFollowing(val actor: MetaActor) : MetaBehavior()
    data class MetaBlocking(val actor: MetaActor) : MetaBehavior()

    open fun copyWithNewActors(newActors: List<MetaActor>): MetaBehavior {
        return when (this) {
            is MetaDrive -> MetaDrive(speedRange)
            is MetaStop -> MetaStop(brakeValue)
            is MetaWait -> MetaWait(timeRange)
            is MetaTurn -> MetaTurn(direction)
            is MetaAccelerate -> MetaAccelerate(accelerationRange)
            is MetaChangeLane -> MetaChangeLane(direction)
            is MetaCrossingStreet -> MetaCrossingStreet(actor)
            is MetaComingFrom -> MetaComingFrom(direction)
            is MetaGoingTo -> MetaGoingTo(direction)
            is MetaStarting -> MetaStarting(startingLane)
            is MetaOvertake -> MetaOvertake(actor)
            is MetaCatchingUp -> MetaCatchingUp(actor)
            is MetaInFrontOf -> MetaInFrontOf(actor)
            is MetaBehind -> MetaBehind(actor)
            is MetaFollowing -> MetaFollowing(actor)
            is MetaBlocking -> MetaBlocking(actor)
        }.apply {
            this.id = this@MetaBehavior.id
            this.actors = newActors
        }
    }

}
