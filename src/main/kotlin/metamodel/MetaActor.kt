package metamodel

import model.Coordinate

open class MetaActor() {
    open var position: Coordinate = Coordinate(0.0,0.0,0.0,0.0)
    open var id: Int = 0

    companion object {
        private var counter = 0
        fun generateUniqueID(): Int = counter++
    }
}