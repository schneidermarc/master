package metamodel

enum class MetaDirection {
    LEFT,
    RIGHT,
    STRAIGHT,
    BACKWARD,
    ACROSS,
    TOP,
    BOTTOM,
    DEFAULT
}