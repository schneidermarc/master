package metamodel

enum class MetaTrafficLight {
    RED,
    GREEN,
    DEFAULT
}