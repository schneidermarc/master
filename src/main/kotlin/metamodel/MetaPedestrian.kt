package metamodel

import model.Coordinate

class MetaPedestrian : MetaActor() {
    var model: String = ""
    override var position: Coordinate = Coordinate(0.0, 0.0, 0.0, 0.0)
    override var id: Int = generateUniqueID()

}