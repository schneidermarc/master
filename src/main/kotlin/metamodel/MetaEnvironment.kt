package metamodel

class MetaEnvironment() {
    var scenarioType: MetaScenarioType = MetaScenarioType.DEFAULT
    var weather: MetaWeather = MetaWeather.DEFAULT
    var trafficLight : MetaTrafficLight = MetaTrafficLight.DEFAULT
}