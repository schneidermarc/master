@file:Suppress("NAME_SHADOWING")

import builders.*
import metamodel.*

@DslMarker
annotation class ScenarioDSL

@ScenarioDSL
fun scenario(init: MetaScenarioBuilder.() -> Unit): MetaScenario {
    val builder = MetaScenarioBuilder()
    builder.init()
    return builder.build()
}












fun main() {
    val startTime = System.nanoTime()
    val scenario = scenario {
        name = "MyScenario"
        amount = 1
        recording = true

        val egoVehicle = ego {}
        val otherVehicle1 = otherVehicle {}
        val otherVehicle2 = otherVehicle{}
        val pedestrian = pedestrian {}

        environment {
            on(junction)
            during(midRainySunset)
        }

        behavior {
            egoVehicle comingFrom left
            otherVehicle1 comingFrom right
            otherVehicle2 behind otherVehicle1
            pedestrian crossingStreetOf otherVehicle1

            egoVehicle turn left
            otherVehicle1 turn left
            otherVehicle2 following otherVehicle1
        }
    }











    println("Generated Scenario:")
    println(scenario)

    val modelTransformator = ModelTransformator()
    val scenarios = modelTransformator.transform(scenario)

    val codeGenerator = CodeGenerator()
    codeGenerator.generateCode(scenarios)

    scenarios.forEachIndexed { index, scenario ->
        println("Scenario: ${index + 1}")
        println("Name: ${scenario.name}")
        println("Weather: ${scenario.environment.weather}")
        println("Scenario Type: ${scenario.environment.scenarioType}")
        println("TrafficLight: ${scenario.environment.trafficLight}")
        println()
    }

    val endTime = System.nanoTime()
    val totalTime = (endTime - startTime) / 1_000_000 // Convert nanoseconds to milliseconds
    println("Total execution time: $totalTime ms")
}