import model.*
import java.io.File

class CodeGenerator {

    private val desktopPath = "C:\\Users\\schne\\Desktop\\Uni\\Masterarbeit\\GenerierteSzenarien\\"

    fun generateCode(scenarios: List<Scenario>) {
        scenarios.forEachIndexed { index, scenario ->
            val pythonCode = generateScenarioCode(scenario)
            val xmlCode = generateXmlCode(scenario)


            val pythonFileName = "${scenario.name}_${index}.py"
            val xmlFileName = "${scenario.name}_${index}.xml"

            saveCodeToFile(pythonCode, pythonFileName)
            saveCodeToFile(xmlCode, xmlFileName)
        }
    }

    private fun generateScenarioCode(scenario: Scenario): String {
        return """
        |${generateImports()}
        |
        |class ${scenario.name}(BasicScenario):
        |
        |    _traffic_light = None
        |    
        |    def __init__(self, world, ego_vehicles, config, randomize=False, debug_mode=False, criteria_enable=True, timeout=60):
        |        self._other_actor_transform = None
        |        self.timeout = timeout
        |        self._blackboard_queue_name = '${scenario.name}/actor_flow_queue'
        |        self._queue = py_trees.blackboard.Blackboard().set(self._blackboard_queue_name, Queue())
        |
        |        super(${scenario.name}, self).__init__("${scenario.name}", ego_vehicles, config, world, debug_mode, criteria_enable=criteria_enable)
        |${generateTrafficLightCode(scenario.environment.trafficLight, scenario)}        
        |
        |    def _initialize_actors(self, config):
        |        # Actor initialization code
        |        self._other_actor_transform = config.other_actors[0].transform
        |${generateActorInitializationCode(scenario)}
        |
        |    def _create_behavior(self):
        |       # Behavior definition code
        |        self._create_weather_data()
        |${generateBehaviorDefinitionsCode(scenario.behaviorTree, scenario)}
        |        
        |       # Behavior tree construction code
        |${generateBehaviorTreeCode(scenario.behaviorTree)}
        |
        |${generateWeatherData(scenario)}    
        |
        |${generateTestCriteria(scenario)}
        |        
        |
        |    def __del__(self):
        |        self.remove_all_actors()
    """.trimMargin()
    }

    private fun getWeatherTypeOfScenario(weather : Weather): Int{
        return when (weather) {
            Weather.DEFAULT -> 0
            Weather.CLEARNOON -> 1
            Weather.CLOUDYNOON -> 2
            Weather.WETNOON -> 3
            Weather.WETCLOUDYNOON -> 4
            Weather.SOFTRAINYNOON -> 5
            Weather.MIDRAINYNOON -> 6
            Weather.HARDRAINYNOON -> 7
            Weather.CLEARSUNSET -> 8
            Weather.CLOUDYSUNSET -> 9
            Weather.WETSUNSET -> 10
            Weather.WETCLOUDYSUNSET -> 11
            Weather.SOFTRAINYSUNSET -> 12
            Weather.MIDRAINYSUNSET -> 13
            Weather.HARDRAINYSUNSET -> 14
        }
    }

    private fun generateWeatherData(scenario: Scenario): String {
        var type = getWeatherTypeOfScenario(scenario.environment.weather)
        return """
        |    def _create_weather_data(self):
        |        tree = ET.parse('C:/Users/schne/Desktop/Uni/Masterarbeit/scenario_runner/srunner/examples/MyScenario.xml')
        |        weatherroot = tree.getroot()
        |        weather_element = weatherroot.find(".//weather")
        |        weather_data = {
        |            "type": $type,  # You can set this to the desired weather type
        |            "cloudiness": float(weather_element.get("cloudiness")),
        |            "precipitation": float(weather_element.get("precipitation")),
        |            "precipitation_deposits": float(weather_element.get("precipitation_deposits")),
        |            "wind_intensity": float(weather_element.get("wind_intensity")),
        |            "sun_azimuth_angle": float(weather_element.get("sun_azimuth_angle")),
        |            "sun_altitude_angle": float(weather_element.get("sun_altitude_angle")),
        |            "fog_density": float(weather_element.get("fog_density")),
        |            "fog_distance": float(weather_element.get("fog_distance")),
        |            "wetness": float(weather_element.get("wetness")),
        |            "fog_falloff": float(weather_element.get("fog_falloff")),
        |            "scattering_intensity": float(weather_element.get("scattering_intensity")),
        |            "mie_scattering_scale": float(weather_element.get("mie_scattering_scale")),
        |            "rayleigh_scattering_scale": float(weather_element.get("rayleigh_scattering_scale")),
        |            "dust_storm": float(weather_element.get("dust_storm"))
        |        }
        |
        |        output_filename = "C:/Users/schne/Desktop/Uni/Masterarbeit/recordings/weather_data__Game_Carla_Maps_Town05_seed${scenario.id}.json"
        |        with open(output_filename, 'w') as json_file:
        |            json.dump(weather_data, json_file, indent=4)
    """.trimMargin()
    }

    private fun generateTrafficLightCode(trafficLight: TrafficLight, scenario: Scenario): String {
        if (trafficLight == TrafficLight.DEFAULT) {
            return ""
        }

        val code = StringBuilder()
        val trafficLightState = when (trafficLight) {
            TrafficLight.GREEN -> "carla.TrafficLightState.Green"
            TrafficLight.RED -> "carla.TrafficLightState.Red"
            else -> throw IllegalArgumentException("Unsupported trafficLight state: $trafficLight")
        }

        val actorCodes = scenario.actors.filterIsInstance<EgoVehicle>()
            .mapIndexed { _, actor -> "self.ego_vehicles[${actor.id}]" } +
                scenario.actors.filterIsInstance<OtherVehicle>()
                    .mapIndexed { _, actor -> "self.other_actors[${actor.id}]" } +
                scenario.actors.filterIsInstance<Motorcade>()
                    .mapIndexed { _, actor -> "self.other_actors[${actor.id}]" }

        val trimmedActorCodes = scenario.actors.filterIsInstance<EgoVehicle>()
            .mapIndexed { _, actor -> "ego${actor.id}" } +
                scenario.actors.filterIsInstance<OtherVehicle>()
                    .mapIndexed { _, actor -> "other${actor.id}" } +
                scenario.actors.filterIsInstance<Motorcade>()
                    .mapIndexed { _, actor -> "other${actor.id}" }



        trimmedActorCodes.forEachIndexed {index, actor ->
            code.appendLine("""
            |        self._traffic_light$actor = CarlaDataProvider.get_next_traffic_light(${actorCodes[index]}, False)
            |
            |        if self._traffic_light$actor is None:
            |            print("No traffic light for the given location of the $actor vehicle found")
            |            sys.exit(-1)
            |        self._traffic_light$actor.set_state($trafficLightState)
            |        self._traffic_light$actor.set_green_time(self.timeout)
        """.trimMargin())}

        return code.toString()
    }

    private fun generateImports() : String {
        val importCode = StringBuilder()
        importCode.appendLine("""
            import sys
            |import py_trees
            |import carla
            |import time
            |from six.moves.queue import Queue
            |from srunner.scenarios.basic_scenario import BasicScenario
            |from srunner.scenariomanager.carla_data_provider import CarlaDataProvider
            |from srunner.scenariomanager.scenarioatomics.atomic_behaviors import *
            |from srunner.scenariomanager.scenarioatomics.atomic_criteria import CollisionTest
            |from srunner.scenariomanager.scenarioatomics.atomic_trigger_conditions import *
            |from srunner.tests.carla_mocks.agents.navigation.local_planner import RoadOption
            |from srunner.tools.scenario_helper import generate_target_waypoint
            |import xml.etree.ElementTree as ET
            |import json
        """.trimIndent())

        return importCode.toString()
    }

    private fun generateActorInitializationCode(scenario: Scenario): String {
        val actorCode = StringBuilder()
        val otherActors = scenario.actors.filterNot { it is EgoVehicle }

        if (otherActors.isNotEmpty()) {
            actorCode.append("""
            |        for actor in config.other_actors:
            |            vehicle = CarlaDataProvider.request_new_actor(actor.model, actor.transform)
            |            self.other_actors.append(vehicle)
            |            vehicle.set_simulate_physics(enabled=False)
            |
        """.trimMargin())
            for (i in otherActors.indices) {
                actorCode.appendLine("""
            |        self._other_actor_transform_$i = config.other_actors[$i].transform
            """.trimMargin())
            }
            actorCode.append("""
            |        first_vehicle_transform = carla.Transform(
            |            carla.Location(config.other_actors[0].transform.location.x,
            |                           config.other_actors[0].transform.location.y,
            |                           config.other_actors[0].transform.location.z - 500),
            |            config.other_actors[0].transform.rotation)
            |
            |        first_vehicle = CarlaDataProvider.request_new_actor(config.other_actors[0].model, first_vehicle_transform)
            |        first_vehicle.set_transform(first_vehicle_transform)
            |        first_vehicle.set_simulate_physics(enabled=False)
            |        self.other_actors.append(first_vehicle)
        """.trimMargin())
        }
        return actorCode.toString()
    }
    private fun generateBehaviorDefinitionsCode(behaviorTree: BehaviorTree, scenario: Scenario): String {
        val codeBuilder = StringBuilder()
        collectBehaviorsFromTree(behaviorTree, codeBuilder, scenario)

        return codeBuilder.toString()
    }

    private fun collectBehaviorsFromTree(tree: BehaviorTree, codeBuilder: StringBuilder, scenario : Scenario) {
        when (tree.type) {
            BehaviorTreeType.BEHAVIOR -> {
                val behavior = tree.behavior ?: error("Behavior node without a behavior object.")
                codeBuilder.append(generateBehaviorDefinitionCode(behavior, scenario))
                codeBuilder.appendLine()
            }
            BehaviorTreeType.SEQUENCE, BehaviorTreeType.PARALLEL -> {
                tree.children.forEach { child ->
                    collectBehaviorsFromTree(child, codeBuilder, scenario)
                }
            }
        }
    }

    private fun generateBehaviorDefinitionCode(behavior: Behavior, scenario:Scenario): String {
        val behaviorName = behavior.name
        val behaviorID = behavior.id
        val actorCodes = behavior.actors.mapIndexed { _, actor ->
            when (actor) {
                is EgoVehicle -> "self.ego_vehicles[${actor.id}]"
                is OtherVehicle -> "self.other_actors[${actor.id}]"
                is Motorcade -> "self.other_actors[${actor.id}]"
                is Cyclist -> "self.other_actors[${actor.id}]"
                is Pedestrian -> "self.other_actors[${actor.id}]"
                is Obstacle -> "self.other_actors[${actor.id}]"
                // Handle other actor types here
                else -> error("Invalid actor type: ${actor.javaClass.simpleName}")
            }
        }
        val behaviorCode = when (behavior) {
            is Stop -> {
                val brakeValue = behavior.brakeValue
                val actorCode = actorCodes.first() // Assuming only one actor for the Stop behavior
                """
        ${behaviorName}_$behaviorID = Stop($actorCode, $brakeValue, "Stopping")"""
            }
            is MotorcadeBehavior -> {
                val actorCode = actorCodes.first()
                val behaviorActor = behavior.actors.first() as Motorcade
                """
        target_waypoint = generate_target_waypoint(
            CarlaDataProvider.get_map().get_waypoint($actorCode.get_location()), 0)
        plan = []
        wp_choice = target_waypoint.next(1.0)

        while not wp_choice[0].is_intersection:
            target_waypoint = wp_choice[0]
            plan.append((target_waypoint, RoadOption.STRAIGHT))
            wp_choice = target_waypoint.next(1.0)
        ${behaviorName}_$behaviorID = ActorSource(['${behaviorActor.model}'],
                                            self._other_actor_transform, 5 , self._blackboard_queue_name, ${behaviorActor.amount})"""
            }
            is Delete -> {
                val actorCode = actorCodes.first() // Assuming only one actor for the Delete behavior
                """
        ${behaviorName}_$behaviorID = ActorDestroy($actorCode)"""
            }
            is DriveStraight -> {
                val speed = behavior.speed
                val actorCode = actorCodes.first() // Assuming only one actor for the DriveStraight behavior
                """
        ${behaviorName}_$behaviorID = KeepVelocity(actor=$actorCode, target_velocity= $speed, duration=float("inf"), distance=float("inf"), name="KeepVelocity")"""
            }
            is Spawn -> {
                val actorCode = actorCodes.first() // Assuming only one actor for the Spawn behavior
                if (actorCode == "self.ego_vehicles[0]"){
                    """"""
                } else {
                    """
        ${behaviorName}_$behaviorID = ActorTransformSetter($actorCode, self._other_actor_transform_${behavior.actors.first().id}, True, "ActorTransformSetter")"""
                }
            }
            is StartRecording -> {
                """
        start_recorder = StartRecorder(f'C:\\Users\\Schne\\Desktop\\Uni\\Masterarbeit\\recordings\\_Game_Carla_Maps_Town05_seed${scenario.id}.log',name="Recorder${scenario.id}")"""
            }
            is StopRecording -> {
                """
        stop_recorder = StopRecorder(name="Recorder${scenario.id}")"""
            }
            is Autopilot -> {
                val actorCode = actorCodes.first()
                """
        target_location_${behaviorName}_$behaviorID = carla.Location(x=${behavior.location.x}, y=${behavior.location.y}, z=${behavior.location.z})
        ${behaviorName}_$behaviorID = BasicAgentBehavior(actor=$actorCode, target_location=target_location_${behaviorName}_$behaviorID, target_speed=${behavior.targetSpeed})"""
            }
            is Following -> {
                val actorCode = actorCodes.first()
                val otherActorCode = actorCodes[1]
                val gap = 15
                """
        ${behaviorName}_$behaviorID = KeepLongitudinalGap(actor=$actorCode, reference_actor=$otherActorCode, gap=$gap, name="Following")"""
            }
            is SyncArrival -> {
                val actorCode = actorCodes.first()
                val otherCode = actorCodes[1]

                """
        ${behaviorName}_$behaviorID = SyncArrival($otherCode ,$actorCode, carla.Location(x=${behavior.targetLocation.x}, y=${behavior.targetLocation.y}, z=${behavior.targetLocation.z}))"""
            }
            is TriggerRegion -> {
                val actorCode = actorCodes.first()
                """
        ${behaviorName}_$behaviorID = InTriggerRegion($actorCode,${behavior.minX}, ${behavior.maxX}, ${behavior.minY}, ${behavior.maxY})"""
            }
            is ChangeLaneLeft -> {
                val actorCode = actorCodes.first()
                """
        ${behaviorName}_$behaviorID = LaneChange(actor=$actorCode, speed=${behavior.velocity}, direction='left')"""
            }
            is WaypointFollowerBehavior -> {
                val actorCode = actorCodes.first()
                """
        ${behaviorName}_$behaviorID = WaypointFollower($actorCode, 6.9, plan=plan, blackboard_queue_name=self._blackboard_queue_name, avoid_collision=True)"""
            }
            is ChangeLaneRight -> {
                val actorCode = actorCodes.first()
                """
        ${behaviorName}_$behaviorID = LaneChange(actor=$actorCode, speed=${behavior.velocity}, direction='right')"""
            }
            is CrossingStreet -> {
                val actorCode = actorCodes.first()
                val pedestrianVelocity = behavior.targetSpeed
                """
        ${behaviorName}_$behaviorID = KeepVelocity(actor = $actorCode, target_velocity = ${pedestrianVelocity},name = "WalkerVelocity")"""
            }
            is AccelerateToCatchUp -> {
                val actorCodeOvertaking = actorCodes[0]
                val actorCodeOvertaken = actorCodes[1]
                """
        ${behaviorName}_$behaviorID = AccelerateToCatchUp(actor=$actorCodeOvertaking , other_actor=$actorCodeOvertaken ,trigger_distance=20, name="AccelerateToCatchUp")"""
            }
            is TriggerDistance -> {
                val actorCode = actorCodes.first()
                val otherActorCode = actorCodes[1]
                """
        ${behaviorName}_$behaviorID = InTriggerDistanceToVehicle(actor=$otherActorCode, reference_actor=$actorCode, distance=${behavior.distance}, name="TriggerDistance")"""
            }
            is TriggerVelocity -> {
                val otherActorCode = actorCodes[1]
                """
        ${behaviorName}_$behaviorID = TriggerVelocity(actor= $otherActorCode, target_velocity= ${behavior.velocity} , name= "TriggerVelocity")"""
            }
            // Handle other behavior types here
            else -> error("Invalid behavior type: ${behavior.javaClass.simpleName}")
        }
        return behaviorCode
    }

    private fun generateBehaviorTreeCode(behaviorTree: BehaviorTree): String {
        val behaviorTreeCode = buildString {
            appendLine("        root = py_trees.composites.Sequence()")
            appendLine("        scenario_actions = py_trees.composites.Parallel(policy=py_trees.common.ParallelPolicy.SUCCESS_ON_ALL)\n")

            val addedNodes = mutableSetOf<BehaviorTree>() // Create a set to track added nodes
            behaviorTree.children.forEach { child ->
                generateBehaviorTreeNodeCode(child, this, addedNodes, null, 8) // Pass the set and set parentNode to null
            }
            appendLine("        root.add_child(scenario_actions)" )
           // appendLine("        recorder = py_trees.composites.Sequence()" )
           // appendLine("        recorder.add_child(start_recorder)" )
           // appendLine("        recorder.add_child(root)" )
            appendLine("        root.add_child(stop_recorder)" )
            appendLine("        return root")
        }
        return behaviorTreeCode.trimMargin()
    }

    private fun generateBehaviorTreeNodeCode(
        node: BehaviorTree,
        codeBuilder: StringBuilder,
        addedNodes: MutableSet<BehaviorTree>,
        parentNode: BehaviorTree? = null,
        indentLevel: Int = 4
    ) {
        val indentation = " ".repeat(indentLevel)
        if (node in addedNodes) return // Node already added, skip
        addedNodes.add(node)

        when (node.type) {
            BehaviorTreeType.BEHAVIOR -> {
                if (parentNode == null) {
                    when (val behavior = node.behavior) {
                        is Spawn -> {
                            if (behavior.actors.first() !is EgoVehicle) {
                                val namespace = "${behavior.name}_${behavior.id}"
                                codeBuilder.appendLine(indentation + "root.add_child(${namespace})")
                            }
                        }
                        is StartRecording -> {
                            codeBuilder.appendLine(indentation + "root.add_child(start_recorder)")
                        }
                        is StopRecording -> {
                            //codeBuilder.appendLine(indentation + "root.add_child(stop_recorder)")
                        }

                        else -> {
                            val namespace = "${behavior?.name}_${behavior?.id}"
                            codeBuilder.appendLine(indentation + "scenario_actions.add_child(${namespace})")
                        }
                    }
                }
            }

            BehaviorTreeType.SEQUENCE -> {
                // Generate code for sequential subtree
                val sequentialName = "sequence_${node.children.first().behavior?.name}_${node.children.first().behavior?.id}"
                codeBuilder.appendLine("$indentation$sequentialName = py_trees.composites.Sequence()")

                // Generate code for children of the sequential subtree
                node.children.forEach { child ->
                    generateBehaviorTreeNodeCode(child, codeBuilder, addedNodes, node, indentLevel)
                    codeBuilder.appendLine("$indentation$sequentialName.add_child(${child.behavior?.name}_${child.behavior?.id})")
                }

                // Add the sequential subtree to the parent node
                codeBuilder.appendLine("${indentation}scenario_actions.add_child($sequentialName)")
            }
            BehaviorTreeType.PARALLEL -> {
                // Generate code for parallel subtree
                val parallelName = "parallel_${node.children.first().behavior?.name}_${node.children.first().behavior?.id}"
                codeBuilder.appendLine("$indentation$parallelName = py_trees.composites.Parallel(policy=py_trees.common.ParallelPolicy.SUCCESS_ON_ONE)")

                // Generate code for children of the parallel subtree
                node.children.forEach { child ->
                    generateBehaviorTreeNodeCode(child, codeBuilder, addedNodes, node, indentLevel)
                    codeBuilder.appendLine("$indentation$parallelName.add_child(${child.behavior?.name}_${child.behavior?.id})")
                }

                // Add the parallel subtree to the parent node
                codeBuilder.appendLine("${indentation}scenario_actions.add_child($parallelName)")
            }
        }
    }

    private fun generateTestCriteria(scenario: Scenario): String{
        var hasEgo = false
        scenario.actors.forEach { actor ->
            if(actor is EgoVehicle){
                hasEgo = true
            }
        }
        val testCriteriaCode = StringBuilder()
        if(hasEgo){
            testCriteriaCode.appendLine( """
        |    def _create_test_criteria(self):
        |        criteria = []
        |        collison_criteria = CollisionTest(self.ego_vehicles[0])
        |        criteria.append(collison_criteria)
        """.trimMargin())
        } else{
            testCriteriaCode.appendLine("""
        |    def _create_test_criteria(self):
        |        criteria = []
        """.trimMargin())
        }

        return testCriteriaCode.toString()
    }

    private fun generateXmlCode(scenario: Scenario): String {
        val codeBuilder = StringBuilder()

        // Generate the XML code based on the scenario object
        codeBuilder.appendLine(
            """<?xml version="1.0"?>
<scenarios>
    <scenario name="${scenario.name}" type="${scenario.name}" town="${scenario.town}">"""
        )

        scenario.actors.forEach { actor ->
            val actorType = when (actor) {
                is EgoVehicle -> "ego_vehicle"
                is OtherVehicle -> "other_actor"
                is Motorcade -> "other_actor"
                is Cyclist -> "other_actor"
                is Pedestrian -> "other_actor"
                is Obstacle -> "other_actor"
                else -> error("Invalid actor type: ${actor.javaClass.simpleName}")
            }
            val location = when (actor) {

                is EgoVehicle -> actor.position
                is OtherVehicle -> actor.position
                is Motorcade -> actor.position
                is Cyclist -> actor.position
                is Pedestrian -> actor.position
                is Obstacle -> actor.position
                else -> error("Invalid actor type: ${actor.javaClass.simpleName}")
            }
            val model = when (actor) {
                is EgoVehicle -> actor.model
                is OtherVehicle -> actor.model
                is Motorcade -> actor.model
                is Cyclist -> actor.model
                is Pedestrian -> actor.model
                is Obstacle -> actor.model
                else -> error("Invalid actor type: ${actor.javaClass.simpleName}")
            }

            codeBuilder.appendLine("""        <$actorType x="${location.x}" y="${location.y}" z="${location.z}" yaw="${location.yaw}" model="$model" />""")
        }

        val weatherParams = translateWeather(scenario)
        val weatherXml = weatherParams.entries.joinToString(" ") { "${it.key}=\"${it.value}\"" }
        codeBuilder.appendLine(
            """        <weather $weatherXml />
    </scenario>
</scenarios>"""
        )

        return codeBuilder.toString()
    }

    private fun translateWeather(scenario: Scenario): Map<String, String> {
        val weatherParams = mutableMapOf<String, String>()

        when (scenario.environment.weather) {
            Weather.DEFAULT -> {
                weatherParams["cloudiness"] = "-1.0"
                weatherParams["precipitation"] = "-1.0"
                weatherParams["precipitation_deposits"] = "-1.0"
                weatherParams["wind_intensity"] = "-1.00"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "-1.0"
                weatherParams["fog_density"] = "-1.0"
                weatherParams["fog_distance"] ="-1.0"
                weatherParams["fog_falloff"] = "-1.0"
                weatherParams["wetness"] = "-1.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.CLEARNOON -> {
                weatherParams["cloudiness"] = "5.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "0.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "45.0"
                weatherParams["fog_density"] = "2.0"
                weatherParams["fog_distance"] = "0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.CLOUDYNOON -> {
                weatherParams["cloudiness"] = "60.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "0.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "45.0"
                weatherParams["fog_density"] = "3.0"
                weatherParams["fog_distance"] = "0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.WETNOON -> {
                weatherParams["cloudiness"] = "5.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "50.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "45.0"
                weatherParams["fog_density"] = "3.0"
                weatherParams["fog_distance"] = "0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.WETCLOUDYNOON -> {
                weatherParams["cloudiness"] = "60.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "50.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "45.0"
                weatherParams["fog_density"] = "3.0"
                weatherParams["fog_distance"] = "0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.SOFTRAINYNOON -> {
                weatherParams["cloudiness"] = "20.0"
                weatherParams["precipitation"] = "30.0"
                weatherParams["precipitation_deposits"] = "50.0"
                weatherParams["wind_intensity"] = "30.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "45.0"
                weatherParams["fog_density"] = "3.0"
                weatherParams["fog_distance"] = "0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.MIDRAINYNOON -> {
                weatherParams["cloudiness"] = "60.0"
                weatherParams["precipitation"] = "60.0"
                weatherParams["precipitation_deposits"] = "60.0"
                weatherParams["wind_intensity"] = "60.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "45.0"
                weatherParams["fog_density"] = "3.0"
                weatherParams["fog_distance"] = "0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.HARDRAINYNOON -> {
                weatherParams["cloudiness"] = "100.0"
                weatherParams["precipitation"] = "100.0"
                weatherParams["precipitation_deposits"] = "90.0"
                weatherParams["wind_intensity"] = "100.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "45.0"
                weatherParams["fog_density"] = "7.0"
                weatherParams["fog_distance"] = "0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.CLEARSUNSET -> {
                weatherParams["cloudiness"] = "5.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "0.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "15.0"
                weatherParams["fog_density"] = "2.0"
                weatherParams["fog_distance"] ="0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.CLOUDYSUNSET -> {
                weatherParams["cloudiness"] = "60.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "0.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "15.0"
                weatherParams["fog_density"] = "3.0"
                weatherParams["fog_distance"] ="0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.WETSUNSET -> {
                weatherParams["cloudiness"] = "5.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "50.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "15.0"
                weatherParams["fog_density"] = "2.0"
                weatherParams["fog_distance"] ="0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.WETCLOUDYSUNSET -> {
                weatherParams["cloudiness"] = "60.0"
                weatherParams["precipitation"] = "0.0"
                weatherParams["precipitation_deposits"] = "50.0"
                weatherParams["wind_intensity"] = "10.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "15.0"
                weatherParams["fog_density"] = "2.0"
                weatherParams["fog_distance"] ="0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.SOFTRAINYSUNSET -> {
                weatherParams["cloudiness"] = "20.0"
                weatherParams["precipitation"] = "30.0"
                weatherParams["precipitation_deposits"] = "50.0"
                weatherParams["wind_intensity"] = "30.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "15.0"
                weatherParams["fog_density"] = "2.0"
                weatherParams["fog_distance"] ="0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.MIDRAINYSUNSET -> {
                weatherParams["cloudiness"] = "60.0"
                weatherParams["precipitation"] = "60.0"
                weatherParams["precipitation_deposits"] = "60.0"
                weatherParams["wind_intensity"] = "60.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "15.0"
                weatherParams["fog_density"] = "3.0"
                weatherParams["fog_distance"] ="0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
            Weather.HARDRAINYSUNSET -> {
                weatherParams["cloudiness"] = "100.0"
                weatherParams["precipitation"] = "100.0"
                weatherParams["precipitation_deposits"] = "90.0"
                weatherParams["wind_intensity"] = "100.0"
                weatherParams["sun_azimuth_angle"] = "-1.0"
                weatherParams["sun_altitude_angle"] = "15.0"
                weatherParams["fog_density"] = "7.0"
                weatherParams["fog_distance"] ="0.75"
                weatherParams["fog_falloff"] = "0.1"
                weatherParams["wetness"] = "0.0"
                weatherParams["scattering_intensity"] = "1.0"
                weatherParams["mie_scattering_scale"] = "0.03"
                weatherParams["rayleigh_scattering_scale"] = "0.0331"
                weatherParams["dust_storm"] = "0.0"
            }
        }
        return weatherParams
    }

    private fun saveCodeToFile(code: String, fileName: String) {
        // Save the generated code to a file on the desktop
        val filePath = desktopPath + fileName
        val file = File(filePath)
        file.writeText(code)
    }
}