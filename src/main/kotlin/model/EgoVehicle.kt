package model

class EgoVehicle(override var id: Int, var model: String, override var position: Coordinate) : Actor(){

    override fun toString(): String {
        return "ID specific: $id Position: $position type: ${this.javaClass.simpleName}"
    }
}