package model

class Cyclist(override var id: Int, var model: String, override var position: Coordinate) : Actor(){

    override fun toString(): String {
        return "ID specific: $id Position: $position"
    }
}