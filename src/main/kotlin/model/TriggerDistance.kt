package model

class TriggerDistance(val actor: Actor, val distance: Int) : Behavior() {
    override val name: String = "TriggerDistance"
}