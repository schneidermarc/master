package model

class TriggerRegion(val minX: Double, val maxX: Double, val minY: Double, val maxY: Double) : Behavior() {
    override val name: String = "TriggerRegion"
}