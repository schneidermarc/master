package model

class BehaviorTree(val type: BehaviorTreeType) {
    val children: MutableList<BehaviorTree> = mutableListOf()
    private var parent: BehaviorTree? = null
    var behavior: Behavior? = null

    fun addChild(child: BehaviorTree) {
        child.parent = this
        children.add(child)
    }
}
