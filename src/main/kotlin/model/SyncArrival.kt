package model

class SyncArrival(
    val targetLocation: Coordinate
) : Behavior() {
    override val name: String = "SyncArrival"
}
