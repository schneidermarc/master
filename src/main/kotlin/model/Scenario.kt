package model

class Scenario(
    val id: Int,
    val name: String,
    val town: String,
    val actors : List<Actor>,
    val environment: Environment,
    val behaviorTree : BehaviorTree
) {
}