package model

abstract class Actor() {
    abstract val id: Int
    abstract var position : Coordinate

    override fun toString(): String {
        return "ID: $id"
    }
}