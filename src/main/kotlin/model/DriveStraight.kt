package model

class DriveStraight(val speed:Int) : Behavior() {
    override val name: String = "DriveStraight"

    override fun toString(): String {
        return "speed: $speed"
    }
}