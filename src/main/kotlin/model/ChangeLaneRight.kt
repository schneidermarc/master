package model

class ChangeLaneRight(val velocity: Int) : Behavior() {
    override val name: String = "ChangeLaneRight"

    override fun toString(): String {
        return "ChangeLaneRight"
    }
}
