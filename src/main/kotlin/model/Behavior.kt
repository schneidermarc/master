package model

abstract class Behavior {
    abstract val name: String
    var id: Int = 0
    var actors: MutableList<Actor> = mutableListOf()

    override fun toString(): String {
        val actorNames = actors.toString()
        return "$actorNames $name"
    }
}