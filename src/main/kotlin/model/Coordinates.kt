package model

data class Coordinate(var x: Double, var y: Double, var z: Double, var yaw: Double) {
    override fun toString(): String {
        return "Coordinate: ($x, $y, $z)"
    }
}