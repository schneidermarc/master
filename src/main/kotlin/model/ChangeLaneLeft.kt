package model

class ChangeLaneLeft(val velocity: Int) : Behavior() {
    override val name: String = "ChangeLaneLeft"

    override fun toString(): String {
        return "ChangeLaneLeft"
    }
}