package model

class Motorcade(override var id: Int, var amount: Int,var model: String, override var position: Coordinate) : Actor(){

    override fun toString(): String {
        return "ID specific: $id Position: $position"
    }
}
