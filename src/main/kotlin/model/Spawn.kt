package model

class Spawn(val location: Coordinate) : Behavior() {
    override val name: String = "Spawn"

    override fun toString(): String {
        return " location: $location, name: $name"
    }
}