package model

enum class BehaviorTreeType {
    SEQUENCE,
    PARALLEL,
    BEHAVIOR
}