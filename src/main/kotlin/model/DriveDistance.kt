package model

class DriveDistance(val distance : Int) : Behavior() {
    override val name: String = "DriveDistance"
}