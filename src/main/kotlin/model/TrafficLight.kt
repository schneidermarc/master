package model

enum class TrafficLight {
    RED,
    GREEN,
    DEFAULT
}