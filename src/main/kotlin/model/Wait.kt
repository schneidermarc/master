package model

data class Wait(val time: Int) : Behavior() {
    override val name: String = "Wait"

    override fun toString(): String {
        return "time: $time"
    }
}