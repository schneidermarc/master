package model

data class Stop(val brakeValue: Double = 1.0) : Behavior() {
    override val name: String = "Stop"
}
