package model

class AccelerateToCatchUp : Behavior() {
    override val name: String = "AccelerateToCatchUp"

    override fun toString(): String {
        return "AccelerateToCatchUp"
    }
}