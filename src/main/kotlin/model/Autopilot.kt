package model

class Autopilot(val location: Coordinate, var targetSpeed:Int) : Behavior() {
    override val name: String = "Autopilot"

    override fun toString(): String {
        return " location: $location, name: $name"
    }
}