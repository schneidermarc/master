package model

data class Accelerate(val acceleration: Int) : Behavior() {
    override val name: String = "Accelerate"

    override fun toString(): String {
        return "acceleration: $acceleration"
    }
}