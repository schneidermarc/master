package model

enum class ScenarioType {
    JUNCTION,
    HIGHWAY,
    ROAD,
    LANE,
    DEFAULT
}