package model

class TriggerVelocity(val actor: Actor, val velocity: Int) : Behavior() {
    override val name: String = "TriggerVelocity"
}