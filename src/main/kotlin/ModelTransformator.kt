import metamodel.*
import model.*
import kotlin.math.roundToInt
import model.BehaviorTreeType

class ModelTransformator {

    private lateinit var followedActor : MetaActor
    private lateinit var followingActor : MetaActor

    private val scenarioTypeToTownMapping = mapOf(
        MetaScenarioType.JUNCTION to "Town05",
        MetaScenarioType.HIGHWAY to "Town04",
        MetaScenarioType.ROAD to "Town03",
        MetaScenarioType.LANE to "Town04"
        // Add mappings for other ScenarioType values
    )

    private var behaviorIDCounter: Int = 0

    fun transform(metaScenario: MetaScenario): List<Scenario> {
        val trafficLight = translateTrafficLight(metaScenario.environment.trafficLight)
        val scenarios = mutableListOf<Scenario>()
        val weather = translateWeather(metaScenario.environment.weather)
        val metaScenarioType = metaScenario.environment.scenarioType
        val town = scenarioTypeToTownMapping.getOrElse(metaScenarioType) {
            error("Unknown MetaScenarioType: $metaScenarioType")
        }
        val scenarioType = translateScenarioType(metaScenario.environment.scenarioType)
        val environment = Environment(scenarioType, weather, trafficLight)
        val modifiedBehaviors = modifyBehaviors(metaScenario.behaviors)

        repeat(metaScenario.amount) {

            // Create the behavior tree
            val behaviorTree = createBehaviorTree(modifiedBehaviors, metaScenario)
            val actors = metaScenario.actors.map { createActor(it) }
            inorderTraversal(behaviorTree)
            val scenario = Scenario(
                id = it,
                name = metaScenario.name,
                environment = environment,
                actors = actors,
                town = town,
                behaviorTree = behaviorTree
            )
            scenarios.add(scenario)
        }
        return scenarios
    }

    private fun inorderTraversal(tree: BehaviorTree?, indent: String = "") {
        if (tree == null) return

        if (tree.type == BehaviorTreeType.BEHAVIOR && tree.children.size == 0) {
            println("$indent  Behavior: ${tree.behavior!!.name}")
        } else {
            println("$indent${tree.type}")

            for (child in tree.children) {
                inorderTraversal(child, "$indent  ")
            }
        }
    }

    private fun modifyBehaviors(behaviors: List<MetaBehavior>): List<MetaBehavior> {
        val modifiedBehaviors = mutableListOf<MetaBehavior>()

        behaviors.forEach { behavior ->
            // Check if it's a MetaFollowing
            if (behavior is MetaBehavior.MetaFollowing) {
                followingActor = behavior.actors.first()
                followedActor = behavior.actors[1]

                val secondActorBehaviors = behaviors.filter { it.actors[0] == followedActor &&
                        it !is MetaBehavior.MetaStarting &&
                        it !is MetaBehavior.MetaInFrontOf &&
                        it !is MetaBehavior.MetaBehind &&
                        it !is MetaBehavior.MetaComingFrom}

                secondActorBehaviors.forEach{ copyBehavior ->
                    val newBehavior = copyBehavior.copyWithNewActors(listOf(followingActor))
                    newBehavior.id = generateUniqueIdentifier()
                    modifiedBehaviors.add(newBehavior)
                }
            } else {
                // Add the behavior as-is
                modifiedBehaviors.add(behavior)
            }
        }

        return modifiedBehaviors
    }

    private fun createBehaviorTree(metaBehaviors: List<MetaBehavior>, metaScenario : MetaScenario): BehaviorTree {
        var triggerVelocity = 5

        val root = BehaviorTree(BehaviorTreeType.SEQUENCE)
        if (metaScenario.recording) {
            val startRecordingBehavior = StartRecording()
            val startRecordingBehaviorNode = BehaviorTree(BehaviorTreeType.BEHAVIOR)
            startRecordingBehaviorNode.behavior = startRecordingBehavior
            root.addChild(startRecordingBehaviorNode)
        }

        for (metaBehavior in metaBehaviors) {
            if(metaBehavior.actors.first() is MetaMotorcade){
                val actors = metaBehavior.actors.map { actor ->
                    createActor(actor)
                }.toMutableList()
                val waypointFollowerBehavior = WaypointFollowerBehavior()
                waypointFollowerBehavior.id = generateUniqueIdentifier()
                waypointFollowerBehavior.actors = actors

                val waypointFollowerBehaviorNode = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                waypointFollowerBehaviorNode.behavior = waypointFollowerBehavior

                val motorcadeBehavior = MotorcadeBehavior()
                motorcadeBehavior.id = generateUniqueIdentifier()
                motorcadeBehavior.actors = actors

                val motorcadeBehaviorNode = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                motorcadeBehaviorNode.behavior = motorcadeBehavior

                val parallelSubtree = BehaviorTree(BehaviorTreeType.PARALLEL)
                parallelSubtree.addChild(motorcadeBehaviorNode)
                parallelSubtree.addChild(waypointFollowerBehaviorNode)

                root.addChild(parallelSubtree)
            }

            when (metaBehavior) {
                is MetaBehavior.MetaFollowing -> {
                    followingActor = metaBehavior.actors.first()
                    followedActor = metaBehavior.actors[1]
                }
                is MetaBehavior.MetaOvertake -> {
                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.SEQUENCE)
                    val triggerVelocityBehavior = TriggerVelocity(createActor(metaBehavior.actors[1]), triggerVelocity)
                    triggerVelocityBehavior.id = generateUniqueIdentifier()
                    triggerVelocityBehavior.actors.addAll(metaBehavior.actors.map{actor ->
                        createActor(actor)
                    })

                    val triggerVelocitySubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    triggerVelocitySubtree.behavior = triggerVelocityBehavior

                    behaviorSubtree.addChild(triggerVelocitySubtree)

                    val accelerateBehavior = AccelerateToCatchUp()
                    val changeLaneLeftBehavior = ChangeLaneLeft(generateTriggerVelocityOffset(triggerVelocity))
                    val changeLaneRightBehavior = ChangeLaneRight(generateTriggerVelocityOffset(triggerVelocity))

                    for (behavior in listOf(accelerateBehavior, changeLaneLeftBehavior, changeLaneRightBehavior)) {
                        val behaviorTree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                        behaviorTree.behavior = behavior
                        val actors: List<Actor> = metaBehavior.actors.map { actor ->
                            createActor(actor)
                        }
                        behavior.actors.addAll(actors)
                        behavior.id = generateUniqueIdentifier()
                        behaviorSubtree.addChild(behaviorTree)
                    }

                    root.addChild(behaviorSubtree)
                }
                is MetaBehavior.MetaCatchingUp -> {
                    val sequenceSubtree = BehaviorTree(BehaviorTreeType.SEQUENCE)
                    val triggerVelocityBehavior = TriggerVelocity(createActor(metaBehavior.actors[1]), triggerVelocity)
                    triggerVelocityBehavior.id = generateUniqueIdentifier()
                    triggerVelocityBehavior.actors.addAll(metaBehavior.actors.map{actor ->
                        createActor(actor)
                    })

                    val triggerVelocitySubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    triggerVelocitySubtree.behavior = triggerVelocityBehavior

                    sequenceSubtree.addChild(triggerVelocitySubtree)

                    val actors: List<Actor> = metaBehavior.actors.map { actor ->
                        createActor(actor)
                    }

                    val accelerateToCatchUp = AccelerateToCatchUp()
                    accelerateToCatchUp.id = generateUniqueIdentifier()
                    accelerateToCatchUp.actors = actors.toMutableList()

                    val accelerateToCatchUpNode = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    accelerateToCatchUpNode.behavior = accelerateToCatchUp

                    sequenceSubtree.addChild(accelerateToCatchUpNode)
                }
                is MetaBehavior.MetaTurn -> {
                    /*var comingFromBehavior = findComingFromBehaviorForActor(metaBehavior.actors.first(), metaScenario.behaviors)
                    var direction = MetaDirection.DEFAULT
                    if (comingFromBehavior == null) {
                        // If comingFromBehavior is null, try to infer the direction from other behaviors
                        metaBehaviors.forEach { behavior ->
                            if (behavior is MetaBehavior.MetaComingFrom){
                                direction = behavior.direction
                            }
                        }

                        // Create comingFromBehavior with the inferred direction
                        comingFromBehavior = MetaBehavior.MetaComingFrom(direction)
                    }
                    val autopilotLocation = determineTurnDirectionForJunction(metaBehavior.direction, comingFromBehavior)

                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.PARALLEL)
                    val autopilotBehavior = Autopilot(autopilotLocation)
                    autopilotBehavior.id = generateUniqueIdentifier()
                    autopilotBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })

                    val behaviorTree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorTree.behavior = autopilotBehavior
                    behaviorSubtree.addChild(behaviorTree)

                    root.addChild(behaviorSubtree)*/
                    var comingFromBehavior = findComingFromBehaviorForActor(metaBehavior.actors.first(), metaScenario.behaviors)

                    // Check if there is no comingFromBehavior
                    if (comingFromBehavior == null) {
                        val behindOrInFrontOfBehavior = findBehindOrInFrontOfBehaviorForActor(metaBehavior.actors.first(), metaScenario.behaviors)

                        // Check if there is a behind or inFrontOf behavior for the actor
                        if (behindOrInFrontOfBehavior != null ) {
                            // Find the comingFromBehavior of the other actor
                            val otherActor = behindOrInFrontOfBehavior.actors.first { it != metaBehavior.actors.first() }
                            comingFromBehavior = findComingFromBehaviorForActor(otherActor, metaScenario.behaviors)
                        }
                        // If no relevant behavior is found, you can handle it here or skip the turn.
                        // For example, you can set a default direction.
                        if (comingFromBehavior == null) {
                            throw UnsupportedOperationException()
                        }
                    }

                    val direction = metaBehavior.direction
                    val autopilotLocation = determineTurnDirectionForJunction(direction, comingFromBehavior)

                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.PARALLEL)
                    val autopilotBehavior = Autopilot(autopilotLocation, generateRandomSpeedAutopilot())
                    autopilotBehavior.id = generateUniqueIdentifier()
                    autopilotBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })

                    val behaviorTree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorTree.behavior = autopilotBehavior
                    behaviorSubtree.addChild(behaviorTree)

                    root.addChild(behaviorSubtree)

                }
                is MetaBehavior.MetaAccelerate -> {
                    val acceleration = if (metaBehavior.accelerationRange.first == metaBehavior.accelerationRange.last) {
                        metaBehavior.accelerationRange.first
                    } else {
                        generateRandomAcceleration(metaBehavior.accelerationRange)
                    }

                    triggerVelocity = acceleration
                    val accelerateBehavior = Accelerate(acceleration)
                    accelerateBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })
                    accelerateBehavior.id = generateUniqueIdentifier()
                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorSubtree.behavior = accelerateBehavior
                    root.addChild(behaviorSubtree)
                }
                is MetaBehavior.MetaComingFrom -> {
                    var spawnLocation = Coordinate(0.0, 0.0, 0.0, 0.0)
                    metaBehavior.actors.forEach { actor ->
                        spawnLocation = determineSpawnLocationForJunction(metaBehavior.direction)
                        actor.position = spawnLocation
                    }

                    createSpawnBehavior(metaBehavior, spawnLocation, root)
                }
                is MetaBehavior.MetaStarting -> {
                    var spawnLocation = Coordinate(0.0, 0.0, 0.0, 0.0)
                    metaBehavior.actors.forEach { actor ->
                        spawnLocation = determineSpawnLocationForHighway(metaBehavior.startingLane)
                        actor.position = spawnLocation
                    }

                    createSpawnBehavior(metaBehavior, spawnLocation, root)
                }
                is MetaBehavior.MetaBehind -> {
                    val spawnLocation = generateRelationalPosition(metaScenario, metaBehavior.actors[1], true)
                    val firstActor = metaBehavior.actors.first()
                    firstActor.position = spawnLocation

                    createSpawnBehavior(metaBehavior, spawnLocation, root)
                }
                is MetaBehavior.MetaInFrontOf -> {
                    val spawnLocation = generateRelationalPosition(metaScenario, metaBehavior.actors[1], false)
                    val firstActor = metaBehavior.actors.first()
                    firstActor.position = spawnLocation
                    createSpawnBehavior(metaBehavior, spawnLocation, root)
                }
                is MetaBehavior.MetaDrive -> {
                    val speed = if (metaBehavior.speedRange.first == metaBehavior.speedRange.last) {
                        (metaBehavior.speedRange.first/3.6).roundToInt()
                    } else {
                        generateRandomSpeed(metaBehavior.speedRange)
                    }
                    triggerVelocity = speed
                    val driveStraightBehavior = DriveStraight(speed)
                    driveStraightBehavior.id = generateUniqueIdentifier()
                    driveStraightBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })

                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorSubtree.behavior = driveStraightBehavior
                    root.addChild(behaviorSubtree)
                }
                is MetaBehavior.MetaChangeLane -> {
                    var behaviorSubtree: BehaviorTree
                    if (metaBehavior.direction == MetaDirection.LEFT) {
                        val changeLaneLeftBehavior = ChangeLaneLeft(generateTriggerVelocityOffset(triggerVelocity))
                        changeLaneLeftBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                            createActor(actor)
                        })

                        changeLaneLeftBehavior.id = generateUniqueIdentifier()
                        behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                        behaviorSubtree.behavior = changeLaneLeftBehavior
                    } else {
                        val changeLaneRightBehavior = ChangeLaneRight(generateTriggerVelocityOffset(triggerVelocity))
                        changeLaneRightBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                            createActor(actor)
                        })

                        changeLaneRightBehavior.id = generateUniqueIdentifier()
                        behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                        behaviorSubtree.behavior = changeLaneRightBehavior
                    }

                    root.addChild(behaviorSubtree)
                }
                is MetaBehavior.MetaCrossingStreet -> {
                    val sequenceSubtree = BehaviorTree(BehaviorTreeType.SEQUENCE)

                    val otherActor = metaBehavior.actors[1]
                    val pedestrianActor = metaBehavior.actors.first()
                    val spawnLocation = determineSpawnLocationForObstacle(metaScenario, pedestrianActor, otherActor)
                    val spawnBehavior = Spawn(spawnLocation)
                    metaBehavior.actors.first().position = spawnLocation
                    spawnBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })
                    spawnBehavior.id = generateUniqueIdentifier()
                    val spawnSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    spawnSubtree.behavior = spawnBehavior

                    root.addChild(spawnSubtree)

                    val triggerDistanceBehavior = TriggerDistance(createActor(metaBehavior.actors[1]), generateRandomTriggerDistance(triggerVelocity))
                    triggerDistanceBehavior.id = generateUniqueIdentifier()
                    triggerDistanceBehavior.actors.addAll(metaBehavior.actors.map{actor ->
                        createActor(actor)
                    })

                    val triggerDistanceSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    triggerDistanceSubtree.behavior = triggerDistanceBehavior
                    sequenceSubtree.addChild(triggerDistanceSubtree)

                    val crossingStreetBehavior = CrossingStreet(generateRandomSpeedForPedestrian())
                    crossingStreetBehavior.id = generateUniqueIdentifier()
                    crossingStreetBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })

                    val crossingStreetSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    crossingStreetSubtree.behavior = crossingStreetBehavior
                    sequenceSubtree.addChild(crossingStreetSubtree)

                    root.addChild(sequenceSubtree)
                }
                is MetaBehavior.MetaGoingTo -> {
                    val parallelSubtree = BehaviorTree(BehaviorTreeType.PARALLEL)

                    val direction = metaBehavior.direction
                    val autopilotLocation = determineAutopilotLocationForJunction(direction)
                    val autopilotBehavior = Autopilot(autopilotLocation, generateRandomSpeedAutopilot())
                    autopilotBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })
                    autopilotBehavior.id = generateUniqueIdentifier()

                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorSubtree.behavior = autopilotBehavior
                    parallelSubtree.addChild(behaviorSubtree)

                    root.addChild(parallelSubtree)
                }
                is MetaBehavior.MetaStop -> {
                    val stopBehavior = Stop(metaBehavior.brakeValue)
                    stopBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })
                    stopBehavior.id = generateUniqueIdentifier()
                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorSubtree.behavior = stopBehavior
                    root.addChild(behaviorSubtree)
                }
                is MetaBehavior.MetaWait -> {
                    val time = if (metaBehavior.timeRange.first == metaBehavior.timeRange.last) {
                        metaBehavior.timeRange.first
                    } else {
                        generateRandomTime(metaBehavior.timeRange)
                    }
                    val waitBehavior = Wait(time)
                    waitBehavior.actors.addAll(metaBehavior.actors.map { actor ->
                        createActor(actor)
                    })
                    waitBehavior.id = generateUniqueIdentifier()
                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorSubtree.behavior = waitBehavior
                    root.addChild(behaviorSubtree)
                }
                is MetaBehavior.MetaBlocking -> {

                    val obstacleActor = metaBehavior.actors.first()

                    val spawnLocation = determineSpawnLocationForObstacle(metaScenario, obstacleActor, metaBehavior.actors[1])
                    val spawnBehavior = Spawn(spawnLocation)
                    spawnBehavior.id = generateUniqueIdentifier()
                    val obstacle = createActor(obstacleActor)
                    obstacleActor.position = spawnLocation
                    spawnBehavior.actors.add(obstacle)

                    val behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
                    behaviorSubtree.behavior = spawnBehavior
                    root.addChild(behaviorSubtree)
                }
                // Add more cases for other MetaBehaviors
            }
        }
        if (metaScenario.recording) {
            val stopRecordingBehavior = StopRecording()
            val stopRecordingBehaviorNode = BehaviorTree(BehaviorTreeType.BEHAVIOR)
            stopRecordingBehaviorNode.behavior = stopRecordingBehavior
            root.addChild(stopRecordingBehaviorNode)
        }
        return root
    }

    private fun findBehindOrInFrontOfBehaviorForActor(actor: MetaActor, behaviors: List<MetaBehavior>): MetaBehavior? {
        val behindOrInFrontOfBehaviors = behaviors.filter {
            it is MetaBehavior.MetaBehind || it is MetaBehavior.MetaInFrontOf
        }

        return behindOrInFrontOfBehaviors.find { behavior ->
            behavior.actors.contains(actor)
        }
    }

    private fun createSpawnBehavior(metaBehavior: MetaBehavior, spawnLocation : Coordinate, root: BehaviorTree){
        val spawnBehavior = Spawn(spawnLocation)
        spawnBehavior.actors.addAll(metaBehavior.actors.map { actor ->
            createActor(actor)
        })
        spawnBehavior.id = generateUniqueIdentifier()
        val behaviorSubtree = BehaviorTree(BehaviorTreeType.BEHAVIOR)
        behaviorSubtree.behavior = spawnBehavior
        root.addChild(behaviorSubtree)
    }

    private fun createActor(metaActor: MetaActor): Actor {
        val actorId = metaActor.id
        val basePosition = metaActor.position
        val position = generateRandomCoordinate(basePosition)

        return when (metaActor) {
            is MetaEgoVehicle -> EgoVehicle(actorId, metaActor.model, position)
            is MetaOtherVehicle -> OtherVehicle(actorId, metaActor.model, position)
            is MetaCyclist -> Cyclist(actorId, metaActor.model, position)
            is MetaPedestrian -> Pedestrian(actorId, metaActor.model, position)
            is MetaMotorcade -> Motorcade(actorId, metaActor.amount, metaActor.model, position)
            is MetaObstacle -> Obstacle(actorId, metaActor.model, position)
            is MetaTraffic -> Traffic(actorId, position)
            else -> throw IllegalArgumentException("No Such Actor")
        }
    }

    private fun generateUniqueIdentifier(): Int {
        return behaviorIDCounter++
    }

    private fun generateRelationalPosition( metaScenario: MetaScenario,otherActor: MetaActor, behindFlag: Boolean): Coordinate {
        val offset = if (!behindFlag){
            20
        } else {
            -20
        }
        val otherPosition = otherActor.position
        return when (metaScenario.environment.scenarioType) {
            MetaScenarioType.HIGHWAY -> Coordinate(otherPosition.x + offset, otherPosition.y, otherPosition.z, otherPosition.yaw)
            MetaScenarioType.JUNCTION -> {
                when (otherPosition) {
                    Coordinate(x = -65.0, y = -4.0, z = 1.0, yaw = 180.0) -> Coordinate(otherPosition.x - offset, otherPosition.y, otherPosition.z, otherPosition.yaw)
                    Coordinate(x = -175.0, y = 6.0, z = 1.0, yaw = 0.0) -> Coordinate(otherPosition.x + offset, otherPosition.y, otherPosition.z, otherPosition.yaw)
                    Coordinate(x = -120.0, y = 70.0, z = 1.0, yaw = 270.0) -> Coordinate(otherPosition.x, otherPosition.y - offset, otherPosition.z, otherPosition.yaw)
                    Coordinate(x = -131.0, y = -70.0, z = 1.0, yaw = 90.0) -> Coordinate(otherPosition.x, otherPosition.y + offset, otherPosition.z, otherPosition.yaw)
                    else -> throw IllegalArgumentException("Unknown position for relation: $otherPosition" + otherActor)
                }
            }
            else -> throw IllegalArgumentException("Unknown scenario type: ${metaScenario.environment.scenarioType}")
        }
    }

    private fun determineSpawnLocationForHighway(startingLane: MetaStartingLane): Coordinate{
        return when (startingLane) {
            MetaStartingLane.FIRSTLANE -> Coordinate(x = -340.0, y = 37.0, z = 1.0, yaw = 0.0)
            MetaStartingLane.SECONDLANE -> Coordinate(x = -340.0, y = 33.5, z = 1.0, yaw = 0.0)
            MetaStartingLane.THIRDLANE -> Coordinate(x = -340.0, y = 30.0, z = 1.0, yaw = 0.0)
            MetaStartingLane.FOURTHLANE -> Coordinate(x = -340.0, y = 26.5, z = 1.0, yaw = 0.0)
        }
    }

    private fun determineSpawnLocationForObstacle(metaScenario: MetaScenario, objectActor:MetaActor, otherActor: MetaActor) : Coordinate {
        return when (metaScenario.environment.scenarioType) {
            MetaScenarioType.HIGHWAY -> {
                when (objectActor) {
                    is MetaObstacle -> Coordinate(x = -270.0, y = otherActor.position.y, z = 2.0, yaw = 270.0)
                    is MetaPedestrian -> Coordinate(x = -270.0, y = 40.0, z = 2.0, yaw = 270.0)
                    else -> throw IllegalArgumentException("No Such Obstacle Object found $objectActor")
                }
            }
            MetaScenarioType.JUNCTION -> {
                val otherActorPosition = otherActor.position
                when {
                    objectActor is MetaPedestrian && otherActorPosition.y == -4.0 -> Coordinate(x = -113.0, y = -11.0, z = 1.0, yaw = 90.0)
                    objectActor is MetaPedestrian && otherActorPosition.x == -120.0 -> Coordinate(x = -113.0, y = 13.0, z = 1.0, yaw = 180.0)
                    objectActor is MetaPedestrian && otherActorPosition.y == 6.0 -> Coordinate(x = -140.0, y = 11.0, z = 1.0, yaw = 270.0)
                    objectActor is MetaPedestrian && otherActorPosition.x == -131.0 -> Coordinate(x = -140.0, y = -11.0, z = 1.0, yaw = 0.0)
                    objectActor is MetaObstacle && otherActorPosition.y == -4.0 -> Coordinate(x = -113.0, y = -4.0, z = 1.0, yaw = 90.0)
                    objectActor is MetaObstacle && otherActorPosition.x == -120.0 -> Coordinate(x = -120.0, y = 13.0, z = 1.0, yaw = 180.0)
                    objectActor is MetaObstacle && otherActorPosition.y == 6.0 -> Coordinate(x = -140.0, y =6.0, z = 1.0, yaw = 270.0)
                    objectActor is MetaObstacle && otherActorPosition.x == -131.0 -> Coordinate(x = -131.0, y = -11.0, z = 1.0, yaw = 0.0)
                    else -> throw IllegalArgumentException("Unsupported ego vehicle position for junction scenario: $otherActorPosition")
                }
            }
            else -> throw IllegalArgumentException("Unsupported scenario type: ${metaScenario.environment.scenarioType}")
        }
    }

    private fun determineSpawnLocationForJunction(direction: MetaDirection): Coordinate {
        return when (direction) {
            MetaDirection.LEFT -> Coordinate(x = -65.0, y = -4.0, z = 1.0, yaw = 180.0)
            MetaDirection.RIGHT -> Coordinate(x = -175.0, y = 6.0, z = 1.0, yaw = 0.0)
            MetaDirection.TOP -> Coordinate(x = -120.0, y = 70.0, z = 1.0, yaw = 270.0)
            MetaDirection.BOTTOM -> Coordinate(x = -131.0, y = -70.0, z = 1.0, yaw = 90.0)
            else -> throw IllegalArgumentException("Unsupported direction for junction scenario: $direction")
        }
    }

    private fun determineAutopilotLocationForJunction(direction: MetaDirection): Coordinate {
        return when (direction) {
            MetaDirection.LEFT -> Coordinate(x = -65.0, y = 6.0, z = 1.0, yaw = 0.0)
            MetaDirection.RIGHT -> Coordinate(x = -175.0, y = -4.0, z = 1.0, yaw = 180.0)
            MetaDirection.TOP -> Coordinate(x = -131.0, y = 70.0, z = 1.0, yaw = 270.0)
            MetaDirection.BOTTOM -> Coordinate(x = -120.0, y = -70.0, z = 1.0, yaw = 90.0)
            else -> throw IllegalArgumentException("Unsupported direction for junction scenario: $direction")
        }
    }

    private fun determineTurnDirectionForJunction(direction: MetaDirection, comingFromBehavior: MetaBehavior.MetaComingFrom?): Coordinate {
        // Check if the actor has a relevant "comingFrom" behavior
        if (comingFromBehavior != null) {
            return when (comingFromBehavior.direction) {
                MetaDirection.LEFT -> {
                    when (direction) {
                        MetaDirection.LEFT ->  Coordinate(x = -131.0, y = 70.0, z = 1.0, yaw = 270.0) // Top
                        MetaDirection.RIGHT -> Coordinate(x = -120.0, y = -70.0, z = 1.0, yaw = 90.0) // Bottom
                        else -> throw IllegalArgumentException("Unsupported direction for junction scenario: $direction")
                    }
                }
                MetaDirection.RIGHT -> {
                    when (direction) {
                        MetaDirection.LEFT -> Coordinate(x = -120.0, y = -70.0, z = 1.0, yaw = 90.0) // Bottom
                        MetaDirection.RIGHT -> Coordinate(x = -131.0, y = 70.0, z = 1.0, yaw = 270.0) // Top
                        else -> throw IllegalArgumentException("Unsupported direction for junction scenario: $direction")
                    }
                }
                MetaDirection.TOP -> {
                    when (direction) {
                        MetaDirection.LEFT -> Coordinate(x = -175.0, y = -4.0, z = 1.0, yaw = 180.0) // Left
                        MetaDirection.RIGHT -> Coordinate(x = -65.0, y = 6.0, z = 1.0, yaw = 0.0) // Right
                        else -> throw IllegalArgumentException("Unsupported direction for junction scenario: $direction")
                    }
                }
                MetaDirection.BOTTOM -> {
                    when (direction) {
                        MetaDirection.LEFT -> Coordinate(x = -65.0, y = 6.0, z = 1.0, yaw = 0.0) // Left
                        MetaDirection.RIGHT -> Coordinate(x = -175.0, y = -4.0, z = 1.0, yaw = 180.0) // Right
                        else -> throw IllegalArgumentException("Unsupported direction for junction scenario: $direction")
                    }
                }
                else -> throw IllegalArgumentException("Unsupported direction for junction scenario: ${comingFromBehavior.direction}")
            }
        }

        // If there is no "comingFrom" behavior, use the original direction for Autopilot location
        return when (direction) {
            MetaDirection.LEFT -> Coordinate(x = -65.0, y = 6.0, z = 1.0, yaw = 0.0)
            MetaDirection.RIGHT -> Coordinate(x = -175.0, y = -4.0, z = 1.0, yaw = 180.0)
            MetaDirection.TOP -> Coordinate(x = -120.0, y = -70.0, z = 1.0, yaw = 90.0)
            MetaDirection.BOTTOM -> Coordinate(x = -131.0, y = 70.0, z = 1.0, yaw = 270.0)
            else -> throw IllegalArgumentException("Unsupported direction for junction scenario: $direction")
        }
    }

    private fun generateRandomCoordinate(baseCoordinate: Coordinate): Coordinate {
        return Coordinate(baseCoordinate.x, baseCoordinate.y, baseCoordinate.z, baseCoordinate.yaw)
    }

    private fun generateRandomTriggerDistance(triggerVelocity: Int) : Int {
        val lowerBound = 5*triggerVelocity
        val upperBound = 7*triggerVelocity
        return (lowerBound ..upperBound).random()
    }

    private fun generateTriggerVelocityOffset(triggerVelocity: Int) : Int {
        val offSet: Double = if (triggerVelocity <= 10) {
            2.5
        } else if(triggerVelocity in 10..15){
            3.5
        } else {
            triggerVelocity/4.0
        }
        return (triggerVelocity+offSet).roundToInt()
    }

    private fun generateRandomSpeed(speedRange: IntRange): Int {
        val speed = (speedRange.first..speedRange.last).random() / 3.6
        return speed.roundToInt()
    }

    private fun generateRandomSpeedForPedestrian(): Int {
        val speed = (5..10).random() / 3.6
        return speed.roundToInt()
    }

    private fun generateRandomSpeedAutopilot(): Int {
        val speed = (25..60).random() / 3.6
        return speed.roundToInt()
    }

    private fun generateRandomTime(timeRange: IntRange): Int {
        return (timeRange.first..timeRange.last).random()
    }

    private fun generateRandomAcceleration(accelerationRange: IntRange): Int {
        return (accelerationRange.first..accelerationRange.last).random()
    }

    private fun translateScenarioType(metaScenarioType: MetaScenarioType): ScenarioType {
        return when (metaScenarioType) {
            MetaScenarioType.JUNCTION -> ScenarioType.JUNCTION
            MetaScenarioType.HIGHWAY -> ScenarioType.HIGHWAY
            MetaScenarioType.ROAD -> ScenarioType.ROAD
            MetaScenarioType.LANE -> ScenarioType.LANE
            MetaScenarioType.DEFAULT -> ScenarioType.DEFAULT
        }
    }

    private fun translateTrafficLight(metaTrafficLight: MetaTrafficLight): TrafficLight {
        return when (metaTrafficLight) {
            MetaTrafficLight.RED -> TrafficLight.RED
            MetaTrafficLight.GREEN -> TrafficLight.GREEN
            MetaTrafficLight.DEFAULT -> TrafficLight.DEFAULT
        }
    }
    private fun translateWeather(metaWeather: MetaWeather): Weather {
        return when (metaWeather) {
            MetaWeather.DEFAULT -> Weather.DEFAULT
            MetaWeather.CLEARNOON -> Weather.CLEARNOON
            MetaWeather.CLOUDYNOON -> Weather.CLOUDYNOON
            MetaWeather.WETNOON -> Weather.WETNOON
            MetaWeather.WETCLOUDYNOON -> Weather.WETCLOUDYNOON
            MetaWeather.SOFTRAINYNOON -> Weather.SOFTRAINYNOON
            MetaWeather.MIDRAINYNOON -> Weather.MIDRAINYNOON
            MetaWeather.HARDRAINYNOON -> Weather.HARDRAINYNOON
            MetaWeather.CLEARSUNSET -> Weather.CLEARSUNSET
            MetaWeather.CLOUDYSUNSET -> Weather.CLOUDYSUNSET
            MetaWeather.WETSUNSET -> Weather.WETSUNSET
            MetaWeather.WETCLOUDYSUNSET -> Weather.WETCLOUDYSUNSET
            MetaWeather.SOFTRAINYSUNSET -> Weather.SOFTRAINYSUNSET
            MetaWeather.MIDRAINYSUNSET -> Weather.MIDRAINYSUNSET
            MetaWeather.HARDRAINYSUNSET -> Weather.HARDRAINYSUNSET
        }
    }

    private fun findComingFromBehaviorForActor(actor: MetaActor, behaviors: List<MetaBehavior>): MetaBehavior.MetaComingFrom? {
        val actorBehaviors = behaviors.filter { it.actors.contains(actor) }
        return actorBehaviors.find { it is MetaBehavior.MetaComingFrom } as? MetaBehavior.MetaComingFrom
    }
}