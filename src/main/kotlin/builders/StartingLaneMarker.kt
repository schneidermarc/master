package builders

import ScenarioDSL

@ScenarioDSL
interface StartingLaneMarker
object firstLane : StartingLaneMarker
object secondLane: StartingLaneMarker
object thirdLane : StartingLaneMarker
object fourthLane : StartingLaneMarker