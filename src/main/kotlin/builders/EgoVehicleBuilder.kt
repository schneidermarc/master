package builders

import ScenarioDSL
import metamodel.MetaEgoVehicle

@ScenarioDSL
class EgoVehicleBuilder : ActorBuilder() {
    private var model: String = "vehicle.tesla.model3"

    fun model(value: String) {
        model = value
    }

    fun build(): MetaEgoVehicle {
        val egoVehicle = MetaEgoVehicle()
        egoVehicle.model = model
        return egoVehicle
    }
}

