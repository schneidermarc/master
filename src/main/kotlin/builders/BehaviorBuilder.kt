package builders

import ScenarioDSL
import metamodel.MetaActor
import metamodel.MetaBehavior
import metamodel.MetaBehavior.*
import metamodel.MetaDirection
import metamodel.MetaStartingLane

@ScenarioDSL
class BehaviorBuilder {
    private val behaviors: MutableList<MetaBehavior> = mutableListOf()
    private var behaviorIDCounter: Int = 0

    private fun generateUniqueIdentifier(): Int {
        return behaviorIDCounter++
    }

    infix fun MetaActor.turn(directionMarker: DirectionMarker) {
        val metaDirection = when (directionMarker) {
            right -> MetaDirection.RIGHT
            left -> MetaDirection.LEFT
            across -> MetaDirection.ACROSS
            straight -> MetaDirection.STRAIGHT
            backward -> MetaDirection.BACKWARD
            else -> throw IllegalArgumentException("Invalid direction: $directionMarker")
            // Add more markers and corresponding MetaDirection values as needed
        }

        val turnBehavior = MetaTurn(metaDirection)
        turnBehavior.id = generateUniqueIdentifier()
        turnBehavior.actors = listOf(this)
        behaviors.add(turnBehavior)
    }

    infix fun MetaActor.comingFrom(directionMarker: DirectionMarker) {
        val metaDirection = when (directionMarker) {
            right -> MetaDirection.RIGHT
            left -> MetaDirection.LEFT
            across -> MetaDirection.ACROSS
            straight -> MetaDirection.STRAIGHT
            backward -> MetaDirection.BACKWARD
            top -> MetaDirection.TOP
            bottom -> MetaDirection.BOTTOM
            else -> throw IllegalArgumentException("Invalid direction: $directionMarker")
            // Add more markers and corresponding MetaDirection values as needed
        }
        val comingFromBehavior = MetaComingFrom(metaDirection)
        comingFromBehavior.id = generateUniqueIdentifier()
        comingFromBehavior.actors = listOf(this)

        behaviors.add(comingFromBehavior)
    }

    infix fun MetaActor.inFrontOf(otherActor: MetaActor) {
        val inFrontOfBehavior = MetaInFrontOf(otherActor)
        inFrontOfBehavior.id = generateUniqueIdentifier()
        inFrontOfBehavior.actors = listOf(this, otherActor)
        behaviors.add(inFrontOfBehavior)
    }

    infix fun MetaActor.following(otherActor: MetaActor) {
        val followingBehavior = MetaFollowing(otherActor)
        followingBehavior.id = generateUniqueIdentifier()
        followingBehavior.actors = listOf(this, otherActor)
        behaviors.add(followingBehavior)
    }

    infix fun MetaActor.behind(otherActor: MetaActor) {
        val behindBehavior = MetaBehind(otherActor)
        behindBehavior.id = generateUniqueIdentifier()
        behindBehavior.actors = listOf(this, otherActor)
        behaviors.add(behindBehavior)
    }


    infix fun MetaActor.startingLane(startingLaneMarker: StartingLaneMarker) {
        val startingLane = when (startingLaneMarker) {
            firstLane -> MetaStartingLane.FIRSTLANE
            secondLane -> MetaStartingLane.SECONDLANE
            thirdLane -> MetaStartingLane.THIRDLANE
            fourthLane -> MetaStartingLane.FOURTHLANE
            else -> throw IllegalArgumentException("Invalid direction: $startingLaneMarker")
            // Add more markers and corresponding MetaDirection values as needed
        }
        val startingBehavior = MetaStarting(startingLane)
        startingBehavior.id = generateUniqueIdentifier()
        startingBehavior.actors = listOf(this)

        behaviors.add(startingBehavior)
    }

    infix fun MetaActor.goingTo(directionMarker: DirectionMarker) {
        val metaDirection = when (directionMarker) {
            right -> MetaDirection.RIGHT
            left -> MetaDirection.LEFT
            across -> MetaDirection.ACROSS
            straight -> MetaDirection.STRAIGHT
            backward -> MetaDirection.BACKWARD
            top -> MetaDirection.TOP
            bottom -> MetaDirection.BOTTOM
            else -> throw IllegalArgumentException("Invalid direction: $directionMarker")
            // Add more markers and corresponding MetaDirection values as needed
        }
        val goingToBehavior = MetaGoingTo(metaDirection)
        goingToBehavior.id = generateUniqueIdentifier()
        goingToBehavior.actors = listOf(this)
        behaviors.add(goingToBehavior)
    }

    infix fun MetaActor.drive(speed: Int) {
        val driveBehavior = MetaDrive(speed..speed)
        driveBehavior.id = generateUniqueIdentifier()
        driveBehavior.actors = listOf(this)
        behaviors.add(driveBehavior)
    }

    infix fun MetaActor.drive(speedRange: IntRange) {
        val driveBehavior = MetaDrive(speedRange)
        driveBehavior.id = generateUniqueIdentifier()
        driveBehavior.actors = listOf(this)
        behaviors.add(driveBehavior)
    }


    fun MetaActor.stop(brakeValue: Double = 1.0) {
        val stopBehavior = MetaStop(brakeValue)
        stopBehavior.id = generateUniqueIdentifier()
        stopBehavior.actors = listOf(this)
        behaviors.add(stopBehavior)
    }

    infix fun MetaActor.wait(duration: Int) {
        val waitBehavior = MetaWait(duration..duration)
        waitBehavior.id = generateUniqueIdentifier()
        waitBehavior.actors = listOf(this)
        behaviors.add(waitBehavior)
    }

    infix fun MetaActor.wait(durationRange: IntRange) {
        val waitBehavior = MetaWait(durationRange)
        waitBehavior.id = generateUniqueIdentifier()
        waitBehavior.actors = listOf(this)
        behaviors.add(waitBehavior)
    }

    infix fun MetaActor.accelerate(acceleration: Int) {
        val accelerateBehavior = MetaAccelerate(acceleration..acceleration)
        accelerateBehavior.id = generateUniqueIdentifier()
        accelerateBehavior.actors = listOf(this)
        behaviors.add(accelerateBehavior)
    }


    infix fun MetaActor.accelerate(accelerationRange: IntRange) {
        val accelerateBehavior = MetaAccelerate(accelerationRange)
        accelerateBehavior.id = generateUniqueIdentifier()
        accelerateBehavior.actors = listOf(this)
        behaviors.add(accelerateBehavior)
    }

    infix fun MetaActor.changeLane(direction: DirectionMarker) {
        val metaDirection = when (direction) {
            left -> MetaDirection.LEFT
            right -> MetaDirection.RIGHT
            else -> throw IllegalArgumentException("Invalid direction: $direction")
            // Add more markers and corresponding MetaDirection values as needed
        }
        val changeLaneBehavior = MetaChangeLane(metaDirection)
        changeLaneBehavior.id = generateUniqueIdentifier()
        changeLaneBehavior.actors = listOf(this)
        behaviors.add(changeLaneBehavior)
    }

    infix fun MetaActor.overtake(otherActor : MetaActor){
        val overtakeBehavior = MetaOvertake(otherActor)
        overtakeBehavior.id = generateUniqueIdentifier()
        overtakeBehavior.actors = listOf(this, otherActor)
        behaviors.add(overtakeBehavior)
    }

    infix fun MetaActor.blocking(otherActor : MetaActor){
        val blockingBehavior = MetaBlocking(otherActor)
        blockingBehavior.id = generateUniqueIdentifier()
        blockingBehavior.actors = listOf(this, otherActor)
        behaviors.add(blockingBehavior)
    }

    infix fun MetaActor.crossingStreetOf(otherActor : MetaActor) {
        val crossingStreetBehavior = MetaCrossingStreet(otherActor)
        crossingStreetBehavior.id = generateUniqueIdentifier()
        crossingStreetBehavior.actors = listOf(this, otherActor)
        behaviors.add(crossingStreetBehavior)
    }

    infix fun MetaActor.catchingUp(otherActor : MetaActor){
        val catchingUpBehavior = MetaCatchingUp(otherActor)
        catchingUpBehavior.id = generateUniqueIdentifier()
        catchingUpBehavior.actors = listOf(this, otherActor)
        behaviors.add(catchingUpBehavior)
    }

    fun build(): List<MetaBehavior> {
        return behaviors
    }
}
