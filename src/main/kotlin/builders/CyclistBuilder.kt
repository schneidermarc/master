package builders

import ScenarioDSL
import metamodel.MetaCyclist

@ScenarioDSL
class CyclistBuilder : ActorBuilder() {
    private var model: String = "vehicle.diamondback.century"

    fun model(value: String) {
        model = value
    }

    fun build(): MetaCyclist {
        val cyclist = MetaCyclist()
        cyclist.model = model
        return cyclist
    }
}
