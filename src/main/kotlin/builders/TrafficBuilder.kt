package builders

import ScenarioDSL
import metamodel.MetaTraffic

@ScenarioDSL
class TrafficBuilder : ActorBuilder() {

    fun build(): MetaTraffic {
        return MetaTraffic()
    }
}
