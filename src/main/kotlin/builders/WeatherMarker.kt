package builders

import ScenarioDSL

@ScenarioDSL
interface WeatherMarker

object default: WeatherMarker //Default
object clearNoon : WeatherMarker //ClearNoon
object cloudyNoon : WeatherMarker // CloudyNoon
object wetNoon: WeatherMarker //WetNoon
object wetCloudyNoon : WeatherMarker //werCloudyNoon
object softRainyNoon : WeatherMarker //softRainNoon
object midRainyNoon :WeatherMarker
object hardRainyNoon :WeatherMarker
object clearSunset : WeatherMarker
object cloudySunset : WeatherMarker
object wetSunset : WeatherMarker
object wetCloudySunset : WeatherMarker
object softRainySunset : WeatherMarker
object midRainySunset : WeatherMarker
object hardRainySunset : WeatherMarker
