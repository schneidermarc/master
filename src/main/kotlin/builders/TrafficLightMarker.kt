package builders

import ScenarioDSL
@ScenarioDSL
interface TrafficLightMarker
object green : TrafficLightMarker
object red : TrafficLightMarker