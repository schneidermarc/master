package builders

import ScenarioDSL
import metamodel.MetaOtherVehicle

@ScenarioDSL
class OtherVehicleBuilder : ActorBuilder() {
    private var model: String = "vehicle.tesla.model3"

    fun model(value: String) {
        model = value
    }

    fun build(): MetaOtherVehicle {
        val otherVehicle = MetaOtherVehicle()
        otherVehicle.model = model
        return otherVehicle
    }
}