package builders

import ScenarioDSL
import metamodel.MetaObstacle

@ScenarioDSL
class ObstacleBuilder : ActorBuilder() {
    private var model: String = "static.prop.vendingmachine"

    fun model(value: String) {
        model = value
    }

    fun build(): MetaObstacle {
        val obstacle = MetaObstacle()
        obstacle.model = model
        return obstacle
    }
}