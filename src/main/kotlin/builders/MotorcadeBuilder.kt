package builders

import ScenarioDSL
import metamodel.MetaMotorcade

@ScenarioDSL
class MotorcadeBuilder : ActorBuilder() {
    private var amount: Int = 1
    private var model: String = "vehicle.tesla.model3"

    fun amount(value: Int) {
        amount = value
    }
    fun model(value: String) {
        model = value
    }

    fun build(): MetaMotorcade {
        val motorcade = MetaMotorcade()
        motorcade.amount = amount
        motorcade.model = model
        return motorcade
    }
}
