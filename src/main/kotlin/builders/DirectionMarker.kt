package builders

import ScenarioDSL

@ScenarioDSL
interface DirectionMarker
object right : DirectionMarker
object left: DirectionMarker
object across : DirectionMarker
object backward : DirectionMarker
object straight: DirectionMarker
object top: DirectionMarker
object bottom: DirectionMarker