package builders

import ScenarioDSL

@ScenarioDSL
interface ScenarioTypeMarker
object junction : ScenarioTypeMarker
object highway: ScenarioTypeMarker
object road : ScenarioTypeMarker
object lane : ScenarioTypeMarker