package builders

import ScenarioDSL
import metamodel.MetaPedestrian

@ScenarioDSL
class PedestrianBuilder : ActorBuilder() {
    private var model: String = "walker.*"

    fun model(value: String) {
        model = value
    }

    fun build(): MetaPedestrian {
        val pedestrian = MetaPedestrian()
        pedestrian.model = model
        return pedestrian
    }
}
