package builders

import ScenarioDSL
import metamodel.*

@ScenarioDSL
class MetaScenarioBuilder(
    var name: String = "",
    var amount: Int = 1,
    var recording: Boolean = false
) {
    private val environmentBuilder = EnvironmentBuilder()
    private val actors: MutableList<MetaActor> = mutableListOf()
    private val behaviors: MutableList<MetaBehavior> = mutableListOf()
    private var environment: MetaEnvironment = MetaEnvironment()

    fun MetaScenarioBuilder.ego(builder: EgoVehicleBuilder.() -> Unit): MetaActor {
        val actorBuilder = EgoVehicleBuilder()
        actorBuilder.builder()
        val actor = actorBuilder.build()
        actors.add(actor)
        return actor
    }

    fun MetaScenarioBuilder.otherVehicle(builder: OtherVehicleBuilder.() -> Unit): MetaActor {
        val actorBuilder = OtherVehicleBuilder()
        actorBuilder.builder()
        val actor = actorBuilder.build()
        actors.add(actor)
        return actor
    }

    fun MetaScenarioBuilder.cyclist(builder: CyclistBuilder.() -> Unit): MetaActor {
        val actorBuilder = CyclistBuilder()
        actorBuilder.builder()
        val actor = actorBuilder.build()
        actors.add(actor)
        return actor
    }

    fun MetaScenarioBuilder.pedestrian(builder: PedestrianBuilder.() -> Unit): MetaActor {
        val actorBuilder = PedestrianBuilder()
        actorBuilder.builder()
        val actor = actorBuilder.build()
        actors.add(actor)
        return actor
    }

    fun MetaScenarioBuilder.obstacle(builder: ObstacleBuilder.() -> Unit): MetaActor {
        val actorBuilder = ObstacleBuilder()
        actorBuilder.builder()
        val actor = actorBuilder.build()
        actors.add(actor)
        return actor
    }

    fun MetaScenarioBuilder.motorcade(builder: MotorcadeBuilder.() -> Unit): MetaActor {
        val actorBuilder = MotorcadeBuilder()
        actorBuilder.builder()
        val actor = actorBuilder.build()
        actors.add(actor)
        return actor
    }

    fun MetaScenarioBuilder.traffic(builder: TrafficBuilder.() -> Unit): MetaActor {
        val actorBuilder = TrafficBuilder()
        actorBuilder.builder()
        val actor = actorBuilder.build()
        actors.add(actor)
        return actor
    }

    fun MetaScenarioBuilder.behavior(builder: BehaviorBuilder.() -> Unit) {
        val behaviorBuilder = BehaviorBuilder()
        behaviorBuilder.builder()
        val behaviors = behaviorBuilder.build()
        this.behaviors.addAll(behaviors)
    }

    fun MetaScenarioBuilder.environment(builder: EnvironmentBuilder.() -> Unit) {
        environmentBuilder.builder()
        val environment = environmentBuilder.build()

        // Set the environment properties
        this.environment.scenarioType = environment.scenarioType
        this.environment.weather = environment.weather
        this.environment.trafficLight = environment.trafficLight
    }

    fun build(): MetaScenario {
        // Build and return the Scenario object
        return MetaScenario(
            name = name,
            amount = amount,
            recording = recording,
            actors = actors,
            behaviors = behaviors,
            environment = environment
        )
    }
}
