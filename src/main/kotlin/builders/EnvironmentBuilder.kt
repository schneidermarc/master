package builders

import metamodel.*
import model.Weather

class EnvironmentBuilder {
    private var scenarioType: MetaScenarioType = MetaScenarioType.DEFAULT
    private var weather: MetaWeather = MetaWeather.DEFAULT
    private var trafficLight : MetaTrafficLight = MetaTrafficLight.DEFAULT

    fun EnvironmentBuilder.on(scenarioMarker: ScenarioTypeMarker) {
        this.scenarioType = when (scenarioMarker) {
            junction -> MetaScenarioType.JUNCTION
            highway -> MetaScenarioType.HIGHWAY
            road -> MetaScenarioType.ROAD
            lane -> MetaScenarioType.LANE
            else -> throw IllegalArgumentException("Invalid scenario type marker: $scenarioMarker")
        }
    }

    fun EnvironmentBuilder.during(weatherMarker: WeatherMarker) {
        this.weather = when (weatherMarker) {
            default -> MetaWeather.DEFAULT
            clearNoon -> MetaWeather.CLEARNOON
            cloudyNoon -> MetaWeather.CLOUDYNOON
            wetNoon -> MetaWeather.WETNOON
            wetCloudyNoon -> MetaWeather.WETCLOUDYNOON
            softRainyNoon -> MetaWeather.SOFTRAINYNOON
            midRainyNoon -> MetaWeather.MIDRAINYNOON
            hardRainyNoon -> MetaWeather.HARDRAINYNOON
            clearSunset -> MetaWeather.CLEARSUNSET
            cloudySunset -> MetaWeather.CLOUDYSUNSET
            wetSunset -> MetaWeather.WETSUNSET
            wetCloudySunset -> MetaWeather.WETCLOUDYSUNSET
            softRainySunset -> MetaWeather.SOFTRAINYSUNSET
            midRainySunset -> MetaWeather.MIDRAINYSUNSET
            hardRainySunset -> MetaWeather.HARDRAINYSUNSET
            else -> throw IllegalArgumentException("Invalid weather marker: $weatherMarker")

        }
    }

    fun EnvironmentBuilder.trafficLight(trafficLightMarker: TrafficLightMarker) {
        this.trafficLight = when (trafficLightMarker) {
            red -> MetaTrafficLight.RED
            green -> MetaTrafficLight.GREEN
            else -> throw IllegalArgumentException("Invalid weather marker: $trafficLightMarker")
        }
    }

    fun build(): MetaEnvironment {
        val environment = MetaEnvironment()
        environment.weather = weather
        environment.scenarioType = scenarioType
        environment.trafficLight = trafficLight

        return environment
    }
}